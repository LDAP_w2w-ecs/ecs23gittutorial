module purge
module load python

python3 -m venv venv --prompt GG

. venv/bin/activate

pip install git+https://gitlab.physik.uni-muenchen.de/LDAP_w2w-ecs/ecs23gitgame.git